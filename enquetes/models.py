from django.db import models

# Create your models here.
class Pergunta(models.Model):
    texto = models.CharField(max_length=200)
    data_publicacao = models.DateTimeField('Data de publicacao')

class Opcao(models.Model):
    texto = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)